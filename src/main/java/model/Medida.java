package model;

public class Medida {

    private Double consumoCalculado;
    private Lectura lecturaDesde;
    private Lectura lecturaHasta;

    public Double getConsumoCalculado() {
        return consumoCalculado;
    }

    public void setConsumoCalculado(Double consumoCalculado) {
        this.consumoCalculado = consumoCalculado;
    }

    public Lectura getLecturaDesde() {
        return lecturaDesde;
    }

    public void setLecturaDesde(Lectura lecturaDesde) {
        this.lecturaDesde = lecturaDesde;
    }

    public Lectura getLecturaHasta() {
        return lecturaHasta;
    }

    public void setLecturaHasta(Lectura lecturaHasta) {
        this.lecturaHasta = lecturaHasta;
    }

    @Override
    public String toString() {
        return "model.Medida{" +
                "consumoCalculado=" + consumoCalculado +
                ", lecturaDesde=" + lecturaDesde +
                ", lecturaHasta=" + lecturaHasta +
                '}';
    }
}
