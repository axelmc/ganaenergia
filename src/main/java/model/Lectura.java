package model;

import java.time.LocalDate;

public class Lectura {

    private LocalDate fecha;
    private Double lecturaValue;

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Double getLecturaValue() {
        return lecturaValue;
    }

    public void setLecturaValue(Double lecturaValue) {
        this.lecturaValue = lecturaValue;
    }

    @Override
    public String toString() {
        return "model.Lectura{" +
                "fecha=" + fecha +
                ", lecturaValue=" + lecturaValue +
                '}';
    }
}
