package util.csv;

import com.opencsv.bean.CsvBindByName;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class MedidasReport {
    @CsvBindByName(column = "Fecha lectura inicial")
    private LocalDate fechaLecturaDesde;
    @CsvBindByName(column = "Valor lectura inicial")
    private Double valorLecturaDesde;
    @CsvBindByName(column = "Fecha lectura final")
    private LocalDate fechaLecturaHasta;
    @CsvBindByName(column = "Valor lectura final")
    private Double valorLecturaHasta;
    @CsvBindByName(column = "Consumo obtenido en XML")
    private Double consumoXml;
    @CsvBindByName(column = "Consumo calculado Java", locale= "es_ES", writeLocale = "es_ES", writeLocaleEqualsReadLocale = false)
    private Double consumoJava;

    public MedidasReport(LocalDate fechaLecturaDesde, Double valorLecturaDesde, LocalDate fechaLecturaHasta, Double valorLecturaHasta, Double consumoXml, Double consumoJava) {
        this.fechaLecturaDesde = fechaLecturaDesde;
        this.valorLecturaDesde = valorLecturaDesde;
        this.fechaLecturaHasta = fechaLecturaHasta;
        this.valorLecturaHasta = valorLecturaHasta;
        this.consumoXml = consumoXml;
        this.consumoJava = consumoJava;
    }

    // Este comparator se usa para ordenar las columnas en el CSV
    public static Comparator<String> getMedidasReportComparator() {
        Comparator<String> comp = new Comparator<String>() {
            Map<String, Integer> myMap = new HashMap<String, Integer>() {{
                put("Fecha lectura inicial".toUpperCase(), 0);
                put("Valor lectura inicial".toUpperCase(), 1);
                put("Fecha lectura final".toUpperCase(), 2);
                put("Valor lectura final".toUpperCase(), 3);
                put("Consumo obtenido en XML".toUpperCase(), 4);
                put("Consumo calculado Java".toUpperCase(), 5);
            }};

            public int compare(String o1, String o2) {
                return myMap.get(o1).compareTo(myMap.get(o2));
            }
        };
        return comp;
    }


}



