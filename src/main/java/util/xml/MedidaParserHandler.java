package util.xml;

import model.Lectura;
import model.Medida;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Stack;

public class MedidaParserHandler extends DefaultHandler
{
    //This is the list which shall be populated while parsing the XML.
    private ArrayList<Medida> medidas = new ArrayList<Medida>();

    //As we read any XML element we will push that in this stack
    private Stack<String> elementStack = new Stack<>();

    //As we complete one user block in XML, we will push the model.Medida instance in medidas
    private Medida medida;

    public void startDocument() throws SAXException
    {
        //System.out.println("start of the document   : ");
    }

    public void endDocument() throws SAXException
    {
        //System.out.println("end of the document document     : ");
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        //Push it in element stack
        this.elementStack.push(qName);

        //If this is start of 'medida' element then prepare a new model.Medida instance and push it in object stack
        if ("Medidas".equals(qName))
        {
            //New model.Medida instance
            medida = new Medida();
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        //Remove last added  element
        this.elementStack.pop();

        //model.Medida instance has been constructed so pop it from object stack and push in medidas
        if ("Medidas".equals(qName))
        {
            this.medidas.add(medida);
        }
    }

    /**
     * This will be called everytime parser encounter a value node
     * */
    public void characters(char[] ch, int start, int length) throws SAXException
    {
        String value = new String(ch, start, length).trim();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (value.length() == 0)
        {
            return; // ignore white space
        }
        if ("ConsumoCalculado".equals(currentElement()))
        {
            medida.setConsumoCalculado(getParsedValue(value));

        }
        else if ("Fecha".equals(currentElement()))
        {
            if (this.elementStack.contains("LecturaDesde")){
                Lectura lecturaDesde = new Lectura();
                lecturaDesde.setFecha(LocalDate.parse(value, formatter));
                medida.setLecturaDesde(lecturaDesde);
            }else{
                Lectura lecturaHasta = new Lectura();
                lecturaHasta.setFecha(LocalDate.parse(value, formatter));
                medida.setLecturaHasta(lecturaHasta);
            }
        }
        else if ("Lectura".equals(currentElement()))        {
            if (this.elementStack.contains("LecturaDesde")){
                medida.getLecturaDesde().setLecturaValue(getParsedValue(value));
            }else{
                medida.getLecturaHasta().setLecturaValue(getParsedValue(value));
            }
        }
    }

    /**
     * Utility method for getting the current element in processing
     * */
    private String currentElement()
    {
        return this.elementStack.peek();
    }

    private Double getParsedValue(String value){
        return  Double.valueOf(value.replaceFirst("^0+(?!$)", ""));
    }

    //Accessor for userList object
    public ArrayList<Medida> getMedidas()
    {
        return medidas;
    }
}