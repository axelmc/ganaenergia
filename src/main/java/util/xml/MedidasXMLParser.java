package util.xml;

import model.Medida;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MedidasXMLParser
{
    public  ArrayList<Medida> parseXml(InputStream in) throws SAXException, IOException {
        //Create a empty link of users initially
        ArrayList<Medida> medidas = new ArrayList<Medida>();
            //Create default handler instance
            MedidaParserHandler handler = new MedidaParserHandler();

            //Create parser from factory
            XMLReader parser = XMLReaderFactory.createXMLReader();

            //Register handler with parser
            parser.setContentHandler(handler);

            //Create an input source from the XML input stream
            InputSource source = new InputSource(in);

            //parse the document
            parser.parse(source);

            //populate the parsed users list in above created empty list; You can return from here also.
            medidas = handler.getMedidas();


        return medidas;
    }
}