package util;

import com.opencsv.CSVWriter;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import model.Medida;
import org.xml.sax.SAXException;
import util.csv.MedidasReport;
import util.xml.MedidasXMLParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Util {

    public static ArrayList<Medida> getMedidasFromXml(String file) {
        // SAX XML Parser
        File xmlFile = new File(file);
        MedidasXMLParser parser = new MedidasXMLParser();
        ArrayList<Medida> medidas = null;

        //Parse the file
        try {
            medidas = parser.parseXml(new FileInputStream(xmlFile));
            System.out.println(medidas);
        } catch (SAXException | IOException e) {
            e.printStackTrace();
            System.out.println("Ha habido un error al parsear el XML");
        }
        return medidas;
    }

    public static void writeCsvReport(ArrayList<Medida> medidas, String csvFile) {
        //OpenCSV 5.1
        try {
            HeaderColumnNameMappingStrategy<MedidasReport> strategy = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(MedidasReport.class);
            strategy.setColumnOrderOnWrite(MedidasReport.getMedidasReportComparator());

            Writer writer = Files.newBufferedWriter(Paths.get(csvFile));

            StatefulBeanToCsv<MedidasReport> csvWriter = new StatefulBeanToCsvBuilder<MedidasReport>(writer)
                    .withMappingStrategy(strategy)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .withOrderedResults(true)
                    .build();

            List<MedidasReport> report = new ArrayList<>();
            medidas.forEach(medida -> {
                MedidasReport row = null;
                double consumoJava = medida.getLecturaHasta().getLecturaValue() - medida.getLecturaDesde().getLecturaValue();

                row = new MedidasReport(
                        medida.getLecturaDesde().getFecha(),
                        medida.getLecturaDesde().getLecturaValue(),
                        medida.getLecturaHasta().getFecha(),
                        medida.getLecturaHasta().getLecturaValue(),
                        medida.getConsumoCalculado(),
                        consumoJava);
                report.add(row);
            });

            //write all medidas to csv file
            csvWriter.write(report);
            writer.close();
            System.out.println("Se ha creado el CSV "+ csvFile);
        } catch (IOException | CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
            System.out.println("Ha habido un error al intentar generar el CSV");
        }
    }

    public static void writeMedidasToMySql(ArrayList<Medida> medidas, String dbUrl, String dbUser, String dbPassword, String dbTable) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
            System.out.println("Conectado con éxito a la base de datos");

            // the mysql insert statement
            String query = " insert into " + dbTable + " (fechaLecturaDesde, valorLecturaDesde, fechaLecturaHasta, valorLecturaHasta, consumoXml, consumoJava )"
                    + " values (?, ?, ?, ?, ?, ?)";
            for (Medida medida : medidas) {
                double consumoJava = medida.getLecturaHasta().getLecturaValue() - medida.getLecturaDesde().getLecturaValue();
                // create the mysql insert PreparedStatement
                PreparedStatement preparedStmt = connection.prepareStatement(query);
                preparedStmt.setDate(1, Date.valueOf(medida.getLecturaDesde().getFecha()));
                preparedStmt.setDouble(2, medida.getLecturaDesde().getLecturaValue());
                preparedStmt.setDate(3, Date.valueOf(medida.getLecturaHasta().getFecha()));
                preparedStmt.setDouble(4, medida.getLecturaHasta().getLecturaValue());
                preparedStmt.setDouble(5, medida.getConsumoCalculado());
                preparedStmt.setDouble(6, consumoJava);

                // execute the PreparedStatement
                preparedStmt.execute();
            }
        } catch (SQLException ex) {
            System.out.println("Error de conexión a la base de datos. Es posible que el servidor no esté en línea");
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}

