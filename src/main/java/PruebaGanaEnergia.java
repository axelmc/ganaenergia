import model.Medida;
import util.Util;

import java.util.ArrayList;

public class PruebaGanaEnergia {
    private static final String XML_FILE ="XML_CANDIDATOS.xml";
    private static final String CSV_FILE = "medidas.csv";
    private static final String DB_URL = "jdbc:mysql://ganaenergia.hopto.org:3306/gana";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "Gana@energia2020";
    private static final String DB_TABLE = "medidas";

    public static void main(String[] args) {

        // Obtenemos las medidas del XML
        ArrayList<Medida> medidas = Util.getMedidasFromXml(XML_FILE);

        // Las escribimos en un CSV
        Util.writeCsvReport(medidas, CSV_FILE);

        // Las guardamos en la base de datos
        Util.writeMedidasToMySql(medidas, DB_URL, DB_USER, DB_PASSWORD, DB_TABLE);
    }
}
